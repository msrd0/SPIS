#include "diff.h"
#include "spisdb.h"
#include "spisnamespace.h"

#include <string.h>

#include <QDebug>
#include <QHash>

using namespace spis;
using namespace spis::driver;

#define DUMMY_COLUMN SPISColumn("dummy", "dummy", -1, SPIS::none, QVariant())


ConstraintDifference::ConstraintDifference(const SPISColumn &a, const SPISColumn &b)
	: _colName(a.name())
{
	Q_ASSERT(a.name() == b.name());
	
	uint8_t both = a.constraints() & b.constraints();
	_constraintsAdded = b.constraints() - both;
	_constraintsRemoved = a.constraints() - both;
}


TableDiff::TableDiff(const SPISTable &a, SPISTable &b)
	: _a(a)
	, _b(&b)
{
	computeDiff();
}

static SPISColumn findCol(const SPISColumn &col, const QHash<QByteArray, SPISColumn> &tbl)
{
	for (auto name : col.alternateNames())
		if (tbl.contains(name))
			return tbl.value(name, DUMMY_COLUMN);
	return DUMMY_COLUMN;
}

void TableDiff::computeDiff()
{
	QHash<QByteArray, SPISColumn> acols, bcols;
	for (auto col : a().columns())
		acols.insert(col.name(), col);
	for (auto col : b().columns())
		bcols.insert(col.name(), col);
	
	// added cols
	for (QByteArray col : bcols.keys())
		if (findCol(bcols.value(col, DUMMY_COLUMN), acols).type() == "dummy")
			_addedCols << bcols.value(col, DUMMY_COLUMN);
	// removed cols
	for (QByteArray col : acols.keys())
		if (findCol(acols.value(col, DUMMY_COLUMN), bcols).type() == "dummy")
			_removedCols << acols.value(col, DUMMY_COLUMN);
	
	// compare cols that are present in both tables
	for (QByteArray colname : acols.keys())
	{
		SPISColumn cola = acols.value(colname, DUMMY_COLUMN);
		Q_ASSERT(cola.type() != "dummy");
		SPISColumn colb = findCol(cola, bcols);
		if (colb.type() == "dummy")
			continue;
		
		QByteArray typea = cola.type(), typeb = colb.type();
		int minsizea = cola.minsize(), minsizeb = colb.minsize();
		
		if ((minsizea != -1 && minsizeb != -1 && minsizea != minsizeb)
				|| (typea != typeb
					&& !(typea == "password" && typeb == "text")
					&& !(typea == "text" && typeb == "password")))
			_typeChanged << colb;
		
		else if (cola.def().toString() != colb.def().toString())
			_defChanged << colb;
		
		else if (cola.constraints() != colb.constraints())
			_constraintsChanged << ConstraintDifference(cola, colb);
	}
}
